import sys
import os
from login import *
from escala import *
from db import *

###################
#VENTANA DE ESCALAS
###################
class VentanaDeEscala(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.centrado()
        self.ui.pushButton.clicked.connect(self.Canjear)
        self.ui.pushButton_2.clicked.connect(self.Update)
        self.ui.pushButton_3.clicked.connect(self.Comprar)

    def centrado(self):
        screen = QtWidgets.QDesktopWidget().screenGeometry()
        size = self.geometry()
        self.move((screen.width() - size.width()) / 2, (screen.height() - size.height()) / 2)

    def Update(self):
        result = query.execute("SELECT Btc FROM PS_UserData.dbo.Donadores WHERE UserID = ? ", (self.ui.NameID.text()))
        for row in result:
            self.ui.BTCNumber.setText(str(row[0]))
            self.ui.progressBar.setProperty("value", row[0])

    def Comprar(self):
        QtGui.QDesktopServices.openUrl(QtCore.QUrl("https://forever-games.com/donacion.html"))

    ###########################
    #Verificar si es dark o luz
    ###########################
    def Canjear(self):
        ######################
        #SELECCIONAR EL PREMIO
        ######################
        ItemsN = [self.ui.radioButton, self.ui.radioButton_2, self.ui.radioButton_3,
                        self.ui.radioButton_4, self.ui.radioButton_5, self.ui.radioButton_6,
                        self.ui.radioButton_7, self.ui.radioButton_8, self.ui.radioButton_9,
                        self.ui.radioButton_10]
        rowItems = 0

        if ItemsN[0].isChecked():
            rowItems = 1
        if ItemsN[1].isChecked():
            rowItems = 2
        if ItemsN[2].isChecked():
            rowItems = 3
        if ItemsN[3].isChecked():
            rowItems = 4
        if ItemsN[4].isChecked():
            rowItems = 5
        if ItemsN[5].isChecked():
            rowItems = 6
        if ItemsN[6].isChecked():
            rowItems = 7
        if ItemsN[7].isChecked():
            rowItems = 8
        if ItemsN[8].isChecked():
            rowItems = 9
        if ItemsN[9].isChecked():
            rowItems = 10

###################################
#VERIFICAR SI EL PREMIO FUE ELEJIDO
###################################
        if rowItems == 0:
            QtWidgets.QMessageBox.warning(self, 'Ops!', 'Por favor seleccione su premio.', QtWidgets.QMessageBox.Ok)
        else:
            ##############################################################
            #VERIFICAR SI EL USUARIO TIENE BTC SUFICIENTE PARA CADA PREMIO
            ##############################################################
            escalas = [1000, 3000, 5000, 8000, 10000, 13000, 16000, 20000, 25000, 30000]
            print(escalas[rowItems -1])
            result = query.execute("SELECT Btc FROM PS_UserData.dbo.Donadores WHERE UserID = ? ", (self.ui.NameID.text()))
            for row in result:
                btcupdater = row[0]

            if btcupdater < escalas[rowItems - 1]:
                QtWidgets.QMessageBox.warning(self, 'Sorry', 'Usted no tiene suficiente BTC para obtener ese premio.', QtWidgets.QMessageBox.Ok)
            else:
                result = query.execute("SELECT UserUID FROM PS_UserData.dbo.Donadores WHERE UserID = ? ", (self.ui.NameID.text()))
                for row in result:
                    nameuid = row[0]
                ###################################################
                #BUSCAR EL PRECIO, CODIGO Y CANTIDAD EN LA DATABASE
                ###################################################
                result = query.execute("SELECT Precio,Codigo,Cantidad FROM PS_UserData.dbo.Escala WHERE RowID = ? ", (rowItems))
                for row in result:
                    precio = row[0]
                    itemsID = row[1]
                    cantidad = row[2]
                ###################################
                #DESCONTAR BTC POR CADA PREMIO DADO
                ###################################
                restaBtc = (btcupdater-precio)
                if restaBtc < 0:
                    restaBtc = 0
                else:
                    query.execute("UPDATE PS_UserData.dbo.Donadores SET Btc = ? WHERE UserUID = ? ", (restaBtc, nameuid))
                    cnx.commit()
                ########################################
                #VERIFICAR SI LA TABLA ESTA VACIA Y SLOT
                ########################################
                result = query.execute("SELECT count(*) FROM PS_GameData.dbo.UserStoredPointItems")
                for row in result:
                    informe = row[0]
                if informe == 0:
                    slot = 0
                else:
                    result = query.execute("SELECT Slot FROM PS_GameData.dbo.UserStoredPointItems WHERE UserUID = ? ", (nameuid))
                    for row in result:
                        slot = row[0]
                        if slot <= 10:
                            slot += 1
                ########################################
                #INSERTAR EL PREMIO AL USUARIO A TU BAUL
                ########################################
                query.execute("INSERT INTO PS_GameData.dbo.UserStoredPointItems (UserUID,Slot,ItemID,ItemCount) VALUES (?, ?, ?, ?)", (nameuid, slot , itemsID, cantidad))
                cnx.commit()
                ###############################
                #ACTUALIZAR SUS BTC TIEMPO REAL
                ###############################
                result = query.execute("SELECT Btc FROM PS_UserData.dbo.Donadores WHERE UserID = ? ", (self.ui.NameID.text()))
                for row in result:
                    self.ui.BTCNumber.setText(str(row[0]))
                    self.ui.progressBar.setProperty("value", row[0])
                    QtWidgets.QMessageBox.information(self, 'Genial!!!', 'Su premio fue enviado a su bodega.', QtWidgets.QMessageBox.Ok)


#################
#VENTANA DE LOGIN
#################
class VentanaLogin(QtWidgets.QDialog):
    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)
        self.centrado()
        self.ui.pushButton.clicked.connect(self.comprobarConexion)
        self.ventanaES = VentanaDeEscala()

    def centrado(self):
        screen = QtWidgets.QDesktopWidget().screenGeometry()
        size = self.geometry()
        self.move((screen.width() - size.width()) / 2, (screen.height() - size.height()) / 2)

    ###############################
    #CONEXION DE LOGIN DON DATABASE
    ###############################
    def comprobarConexion(self):
        if self.ui.lineEdit.text() == "":
            self.ui.lineEdit.setStyleSheet("border: 2px solid yellow;")
            QtWidgets.QMessageBox.warning(self, 'Error', 'Usuario se encuentra vacio.', QtWidgets.QMessageBox.Ok)
        elif self.ui.lineEdit_2.text() == "":
            self.ui.lineEdit_2.setStyleSheet("border: 2px solid yellow;")
            QtWidgets.QMessageBox.warning(self, 'Error', 'Clave se encuentra vacio.', QtWidgets.QMessageBox.Ok)
        else:
            if self.ui.lineEdit.text().isalnum() == True and self.ui.lineEdit_2.text().isalnum() == True:
                user = self.ui.lineEdit.text()
                passw = self.ui.lineEdit_2.text()
                resul = query.execute("SELECT UserID, Passwd FROM PS_UserData.dbo.Donadores WHERE UserID = ? AND Passwd = ? ", (user,passw))
                if(len(resul.fetchall()) > 0):
                    self.ventanaES.show()
                    self.InformacionDelPersonaje()
                    self.close()
                else:
                    QtWidgets.QMessageBox.warning(self, 'Error de usuario', 'Usuario y Clave no son validos.', QtWidgets.QMessageBox.Ok)
            else:
                self.ui.lineEdit.setStyleSheet("border: 2px solid red;")
                self.ui.lineEdit_2.setStyleSheet("border: 2px solid red;")
                QtWidgets.QMessageBox.warning(self, 'Error', 'Solo letras y numeros, nada de espacio o caracteres como: !"#/()""', QtWidgets.QMessageBox.Ok)


    ####################################
    #INFORMACION DEL JUGADOR A LA ESCALA
    ####################################
    def InformacionDelPersonaje(self):
        user = self.ui.lineEdit.text()
        resul = query.execute("SELECT UserUID, UserID, Btc Point FROM PS_UserData.dbo.Donadores WHERE UserID = ? ", (user))
        row = resul.fetchone()
        self.ventanaES.ui.NameID.setText(row[1])
        self.ventanaES.ui.BTCNumber.setText(str(row[2]))
        self.ventanaES.ui.progressBar.setProperty("value", row[2])

        ######################
        #BUSCAR PJS CON EL UID
        ######################
        nameuid = str(row[0])
        resul = query.execute("SELECT CharName FROM PS_GameData.dbo.Chars WHERE UserUID = ? AND Del = ? ", (nameuid, 0))
        labels = [self.ventanaES.ui.PJ1,
                self.ventanaES.ui.PJ2,
                self.ventanaES.ui.PJ3,
                self.ventanaES.ui.PJ4,
                self.ventanaES.ui.PJ5]
        for i, row in enumerate(resul):
            labels[i].setText(row[0])
            acomulador =+ i

        #######################
        #VER EL TIPO DE FACCION
        #######################
        luz = 0
        Lfaccion = [self.ventanaES.ui.Faccion,
                    self.ventanaES.ui.Faccion_2,
                    self.ventanaES.ui.Faccion_3,
                    self.ventanaES.ui.Faccion_4,
                    self.ventanaES.ui.Faccion_5]

        contry = query.execute("SELECT Country FROM PS_GameData.dbo.UserMaxGrow WHERE UserUID = ? ", (nameuid))
        for row in contry:
            for j in range(acomulador + 1):
                if row[0] == luz:
                    Lfaccion[j].setPixmap(QtGui.QPixmap("icons/luz.png"))
                else:
                    Lfaccion[j].setPixmap(QtGui.QPixmap("icons/fury.png"))


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    myapp = VentanaLogin()
    myapp.show()
    sys.exit(app.exec_())
